//
//  LocationModel.h
//  myGeoLocation
//
//  Created by Guilherme Martins on 25/06/17.
//  Copyright © 2017 Guilherme Martins. All rights reserved.
//
//Module
@import UIKit;

//Realm
#import <Realm/Realm.h>

@interface LocationModel : RLMObject

@property (nonatomic, strong) NSString *uuid;
@property (nonatomic, strong) NSString *latitude;
@property (nonatomic, strong) NSString *longitude;
@property (nonatomic, strong) NSString *timestamp;
@end
