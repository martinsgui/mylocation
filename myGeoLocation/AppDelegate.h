//
//  AppDelegate.h
//  myGeoLocation
//
//  Created by Guilherme Martins on 24/06/17.
//  Copyright © 2017 Guilherme Martins. All rights reserved.
//
//Module
@import UIKit;

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

