//
//  ViewController.m
//  myGeoLocation
//
//  Created by Guilherme Martins on 24/06/17.
//  Copyright © 2017 Guilherme Martins. All rights reserved.
//

#import "ViewController.h"

@interface ViewController ()
@property (nonatomic, weak) IBOutlet UIButton *buttonLocation;
@property (nonatomic, weak) IBOutlet UISlider *sliderTimeInterval;
@property (nonatomic, weak) IBOutlet UILabel *intervalLabel;


@property (nonatomic, strong) LocationManager *locationManager;
@property (nonatomic, readwrite) BOOL isLocationStarted;
@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.locationManager = [LocationManager new];
    self.locationManager.delegate = self;
    
    [self.locationManager requestAuthorizationLocation];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Action Method
- (IBAction)tapUserLocation:(id)sender {
    if(self.isLocationStarted) {
        self.isLocationStarted = NO;
        [self.buttonLocation setTitle:NSLocalizedString(@"button.start.label", nil) forState:UIControlStateNormal];
        [self.locationManager stopUserLocationWithInterval];
    } else {
        self.isLocationStarted = YES;
        
        [self.buttonLocation setTitle:NSLocalizedString(@"button.stop.label", nil) forState:UIControlStateNormal];
        [self.locationManager initUserLocationWithInterval:roundl(self.sliderTimeInterval.value)];
    }
}

-(IBAction)intervalValueChanged:(UISlider*)sender {
    self.intervalLabel.text = [NSString stringWithFormat:@"%.f", [sender value]];
    self.locationManager.checkLocationInterval = roundl([sender value]);
}

#pragma mark - Error AlertView
- (void)showAlertWithMessage:(NSString *)message {
    UIAlertController *alertController = [UIAlertController alertControllerWithTitle:NSLocalizedString(@"default.error.title", nil) message:message preferredStyle:UIAlertControllerStyleAlert];
    
    [alertController addAction:[UIAlertAction actionWithTitle:NSLocalizedString(@"ok.button", nil) style:UIAlertActionStyleDefault handler:nil]];
    
    [self presentViewController:alertController animated:YES completion:nil];
}

#pragma mark - LocationManager Methods Delegate
- (void)locationManageDidFailWithError:(NSError *)error {
    NSLog(@"Error: %@",error);
}

- (void)locationManageDidStopLocations:(RLMResults *)locations {
    NSLog(@"jSON: %@",locations);
}
@end
