//
//  LocationManager.h
//  myGeoLocation
//
//  Created by Guilherme Martins on 24/06/17.
//  Copyright © 2017 Guilherme Martins. All rights reserved.
//
//Module
@import Foundation;
@import CoreLocation;

//Model
#import "LocationModel.h"

@protocol LocationManagerDelegate <NSObject>
@required
- (void)locationManageDidFailWithError:(NSError*)error;
- (void)locationManageDidStopLocations:(RLMResults*)locations;
@end

@interface LocationManager : NSObject <CLLocationManagerDelegate>
@property (nonatomic, weak) id <LocationManagerDelegate> delegate;
@property (nonatomic) int checkLocationInterval;

- (void)initUserLocationWithInterval:(int)interval;
- (void)stopUserLocationWithInterval;
- (void)requestAuthorizationLocation;

@end
