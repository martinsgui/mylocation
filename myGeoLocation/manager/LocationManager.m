//
//  LocationManager.m
//  myGeoLocation
//
//  Created by Guilherme Martins on 24/06/17.
//  Copyright © 2017 Guilherme Martins. All rights reserved.
//

#import "LocationManager.h"

//Module
@import CoreLocation;
@import UIKit;

@interface LocationManager ()
@property (nonatomic) UIBackgroundTaskIdentifier bgTask;
@property (nonatomic, strong) CLLocationManager *locationManager;
@property (nonatomic, strong) NSTimer *checkLocationTimer;
@property (nonatomic, strong) NSTimer *waitForLocationUpdatesTimer;
@property (nonatomic, strong) LocationModel *selectedDataObject;
@end

@implementation LocationManager
int const kTimeGetLocation = 3;
int const kMaxBGTime = 170;
NSString * const kFormatDate = @"yyyy-MM-dd HH:mm:ss";

- (id)init {
    if (self = [super init]) {
        self.locationManager = [CLLocationManager new];
        self.locationManager.delegate = self;
        self.locationManager.desiredAccuracy = kCLLocationAccuracyBest;
        self.locationManager.distanceFilter = kCLDistanceFilterNone;
        
        //Refresh in Background App
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(applicationDidEnterBackground:) name:UIApplicationDidEnterBackgroundNotification object:nil];
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(applicationDidBecomeActive:) name:UIApplicationDidBecomeActiveNotification object:nil];
    }
    return self;
}

- (void)initUserLocationWithInterval:(int)interval {
    self.checkLocationInterval = (interval > kMaxBGTime)? kMaxBGTime : interval;
    [self.locationManager startUpdatingLocation];
}

- (void)stopUserLocationWithInterval {
    [self.locationManager stopUpdatingLocation];
    [self stopCheckLocationTimer];
    [self loadDataIntoDataBase];
}

- (void)timerEvent:(NSTimer*)timer {
    [self stopCheckLocationTimer];
    [self.locationManager startUpdatingLocation];
}

- (void)startCheckLocationTimer {
    [self stopCheckLocationTimer];
    self.checkLocationTimer = [NSTimer scheduledTimerWithTimeInterval:self.checkLocationInterval target:self selector:@selector(timerEvent:) userInfo:NULL repeats:NO];
}

- (void)stopCheckLocationTimer {
    if(self.checkLocationTimer){
        [self.checkLocationTimer invalidate];
        self.checkLocationTimer = nil;
    }
}

- (void)startBackgroundTask {
    [self stopBackgroundTask];
    self.bgTask = [[UIApplication sharedApplication] beginBackgroundTaskWithExpirationHandler:^{
        //in case bg task is killed faster than expected, try to start Location Service
        [self timerEvent:self.checkLocationTimer];
    }];
}

- (void)stopBackgroundTask {
    if(self.bgTask != UIBackgroundTaskInvalid){
        [[UIApplication sharedApplication] endBackgroundTask:self.bgTask];
        self.bgTask = UIBackgroundTaskInvalid;
    }
}

- (void)stopWaitForLocationUpdatesTimer {
    if(self.waitForLocationUpdatesTimer){
        [self.waitForLocationUpdatesTimer invalidate];
        self.waitForLocationUpdatesTimer = nil;
    }
}

- (void)startWaitForLocationUpdatesTimer {
    [self stopWaitForLocationUpdatesTimer];
    self.waitForLocationUpdatesTimer = [NSTimer scheduledTimerWithTimeInterval:kTimeGetLocation target:self selector:@selector(waitForLocations:) userInfo:NULL repeats:NO];
}

- (void)waitForLocations:(NSTimer*)theTimer {
    [self stopWaitForLocationUpdatesTimer];
    
    if(([[UIApplication sharedApplication ]applicationState]==UIApplicationStateBackground ||
        [[UIApplication sharedApplication ]applicationState]==UIApplicationStateInactive) &&
       self.bgTask==UIBackgroundTaskInvalid) {
        [self startBackgroundTask];
    }
    
    [self startCheckLocationTimer];
    [self.locationManager stopUpdatingLocation];
}

#pragma mark - CLLocationManagerDelegate methods
- (void)locationManager:(CLLocationManager *)manager didUpdateLocations:(NSArray *)locations {
    if(self.checkLocationTimer){
        //sometimes it happens that location manager does not stop even after stopUpdationLocations
        return;
    }

    CLLocation *location = [locations objectAtIndex:0];
    [self insertDataIntoDataBaseWithLocations:location];
    
    if (!self.waitForLocationUpdatesTimer){
        [self startWaitForLocationUpdatesTimer];
    }
}

- (void)locationManager:(CLLocationManager *)manager didFailWithError:(NSError *)error {
    if (self.delegate && [self.delegate respondsToSelector:@selector(locationManageDidFailWithError:)]) {
        [self.delegate locationManageDidFailWithError:error];
    }
}

#pragma mark - APP Background
- (void)applicationDidEnterBackground:(NSNotification *)notification {
    if([self isLocationServiceAvailable]) {
        [self startBackgroundTask];
    }
}

- (void)applicationDidBecomeActive:(NSNotification *)notification {
    [self stopBackgroundTask];
    if(![self isLocationServiceAvailable]){
        NSError *error = [NSError errorWithDomain:NSLocalizedString(@"default.domain", nil) code:1 userInfo:[NSDictionary dictionaryWithObject:NSLocalizedString(@"location.service.off.title", nil) forKey:NSLocalizedDescriptionKey]];
        
        if (self.delegate && [self.delegate respondsToSelector:@selector(locationManageDidFailWithError:)]) {
            [self.delegate locationManageDidFailWithError:error];
        }
    }
}

#pragma mark - Helpers
- (BOOL)isLocationServiceAvailable {
    if(![CLLocationManager locationServicesEnabled] || [CLLocationManager authorizationStatus]==kCLAuthorizationStatusDenied || [CLLocationManager authorizationStatus]==kCLAuthorizationStatusRestricted) {
        return NO;
    }
    return YES;
}

- (void)requestAuthorizationLocation {
    if([self.locationManager respondsToSelector:@selector(requestWhenInUseAuthorization)]) {
        [self.locationManager requestWhenInUseAuthorization];
    }
}

#pragma mark - UserData Realm
- (void)insertDataIntoDataBaseWithLocations:(CLLocation *)location {
    RLMRealm *realm = [RLMRealm defaultRealm];
    
    [realm beginWriteTransaction];
    LocationModel *model = [LocationModel new];
    model.uuid = [self uniqueIDString];
    model.latitude = [NSString stringWithFormat:@"%.7f", location.coordinate.latitude];
    model.longitude = [NSString stringWithFormat:@"%.7f", location.coordinate.longitude];
    
    NSDateFormatter *formatter = [NSDateFormatter new];
    [formatter setDateFormat:kFormatDate];
    model.timestamp = [formatter stringFromDate:location.timestamp];
    
    [realm addObject:model];
    [realm commitWriteTransaction];
}

- (void)loadDataIntoDataBase {
    if (self.delegate && [self.delegate respondsToSelector:@selector(locationManageDidStopLocations:)]) {
        RLMResults *results =  [LocationModel allObjects];
        [self.delegate locationManageDidStopLocations:results];
    }
}

- (NSString *)uniqueIDString {
    UIDevice *currentDevice = [UIDevice currentDevice];
    return [[currentDevice identifierForVendor] UUIDString];
}

@end
